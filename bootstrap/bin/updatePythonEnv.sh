#! /usr/bin/env bash

venv=${VIRTUAL_ENV:?This is currently not a Python3 venv!  Will not upgrade venv packages.}
echo -e "\nFound a Python3 venv in \"${VIRTUAL_ENV}\".\nWill now proceed with package upgrades.\n"

pip="python3 -m pip"
upgrade_command="${pip} install --upgrade --prefer-binary"

if [ -z ${OS} ]; then
    OS=$(uname)
fi
if [ ${OS} = Darwin ]; then
    find=gfind
else
    find=find
fi

function upgrade()
{
    if [ ${#} -ge 1 ]; then
        echo "Upgrading ${@}..."
        ${upgrade_command} ${@}
        if [ ${?} -eq 0 ]; then
            echo -e "\nUpgrading ${@} done.\n\nNow execute\n\tdeactivate\nand\n\t. ${venv}/bin/activate\n"
            return 1
        else
            echo -e "\nSomething went wrong during the upgrade.\nCheck the output above!\n"
            return -1
        fi
    else
        echo -e "\nNo package for upgrading provided.\n"
        return -2
    fi
    return 0
}

upgrade pip wheel
if [ ${?} -eq 1 ]; then
    read -n 1 -p "If you would like to use the new pip and wheel packages for the package updates, press \"Y\" to exit. Any other key will just continue with the updates. " YESNO
    if [ "${YESNO}" = "Y" ]; then
        echo -e "\n Good bye!\nNow it is time to run this script again.\n"
        exit 0
    else
        echo -e "\nContinuing with the package updates.\n"
    fi
fi

installed_packages=$(for i in $(python3 -m pip freeze | cut -d'=' -f1); do printf "%s " ${i}; done)
upgrade ${installed_packages}
