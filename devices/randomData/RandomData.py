# -*- coding: utf-8 -*-
#
# This file is part of the RandomData project
#
#
#
# Distributed under the terms of the BSD3 license.
# See LICENSE.txt for more info.

""" Random data monitor point device

"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(RandomData.additionnal_import) ENABLED START #
import numpy
# PROTECTED REGION END #    //  RandomData.additionnal_import

__all__ = ["RandomData", "main"]


class RandomData(Device):
    """
    """
    # PROTECTED REGION ID(RandomData.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  RandomData.class_variable

    # ----------
    # Attributes
    # ----------

    rnd1 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd2 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd3 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd4 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd5 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd6 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd7 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd8 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd9 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd10 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd11 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd12 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd13 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd14 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd15 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd16 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd17 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd18 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd19 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd20 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd100 = attribute(
        dtype=('DevDouble',),
        max_dim_x=1000,
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd1000 = attribute(
        dtype=(('DevDouble',),),
        max_dim_x=1000, max_dim_y=1000,
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the RandomData."""
        Device.init_device(self)
        self.rnd1.set_data_ready_event(True)
        self.set_change_event("rnd1", True, True)
        self.set_archive_event("rnd1", True, True)
        self.rnd2.set_data_ready_event(True)
        self.set_change_event("rnd2", True, True)
        self.set_archive_event("rnd2", True, True)
        self.rnd3.set_data_ready_event(True)
        self.set_change_event("rnd3", True, True)
        self.set_archive_event("rnd3", True, True)
        self.rnd4.set_data_ready_event(True)
        self.set_change_event("rnd4", True, True)
        self.set_archive_event("rnd4", True, True)
        self.rnd5.set_data_ready_event(True)
        self.set_change_event("rnd5", True, True)
        self.set_archive_event("rnd5", True, True)
        self.rnd6.set_data_ready_event(True)
        self.set_change_event("rnd6", True, True)
        self.set_archive_event("rnd6", True, True)
        self.rnd7.set_data_ready_event(True)
        self.set_change_event("rnd7", True, True)
        self.set_archive_event("rnd7", True, True)
        self.rnd8.set_data_ready_event(True)
        self.set_change_event("rnd8", True, True)
        self.set_archive_event("rnd8", True, True)
        self.rnd9.set_data_ready_event(True)
        self.set_change_event("rnd9", True, True)
        self.set_archive_event("rnd9", True, True)
        self.rnd10.set_data_ready_event(True)
        self.set_change_event("rnd10", True, True)
        self.set_archive_event("rnd10", True, True)
        self.rnd11.set_data_ready_event(True)
        self.set_change_event("rnd11", True, True)
        self.set_archive_event("rnd11", True, True)
        self.rnd12.set_data_ready_event(True)
        self.set_change_event("rnd12", True, True)
        self.set_archive_event("rnd12", True, True)
        self.rnd13.set_data_ready_event(True)
        self.set_change_event("rnd13", True, True)
        self.set_archive_event("rnd13", True, True)
        self.rnd14.set_data_ready_event(True)
        self.set_change_event("rnd14", True, True)
        self.set_archive_event("rnd14", True, True)
        self.rnd15.set_data_ready_event(True)
        self.set_change_event("rnd15", True, True)
        self.set_archive_event("rnd15", True, True)
        self.rnd16.set_data_ready_event(True)
        self.set_change_event("rnd16", True, True)
        self.set_archive_event("rnd16", True, True)
        self.rnd17.set_data_ready_event(True)
        self.set_change_event("rnd17", True, True)
        self.set_archive_event("rnd17", True, True)
        self.rnd18.set_data_ready_event(True)
        self.set_change_event("rnd18", True, True)
        self.set_archive_event("rnd18", True, True)
        self.rnd19.set_data_ready_event(True)
        self.set_change_event("rnd19", True, True)
        self.set_archive_event("rnd19", True, True)
        self.rnd20.set_data_ready_event(True)
        self.set_change_event("rnd20", True, True)
        self.set_archive_event("rnd20", True, True)
        self.rnd100.set_data_ready_event(True)
        self.set_change_event("rnd100", True, True)
        self.set_archive_event("rnd100", True, True)
        self.rnd1000.set_data_ready_event(True)
        self.set_change_event("rnd1000", True, True)
        self.set_archive_event("rnd1000", True, True)
        # PROTECTED REGION ID(RandomData.init_device) ENABLED START #
        self._rnd1 = numpy.random.random()
        self._rnd2 = numpy.random.random()
        self._rnd3 = numpy.random.random()
        self._rnd4 = numpy.random.random()
        self._rnd5 = numpy.random.random()
        self._rnd6 = numpy.random.random()
        self._rnd7 = numpy.random.random()
        self._rnd8 = numpy.random.random()
        self._rnd9 = numpy.random.random()
        self._rnd10 = numpy.random.random()
        self._rnd11 = numpy.random.random()
        self._rnd12 = numpy.random.random()
        self._rnd13 = numpy.random.random()
        self._rnd14 = numpy.random.random()
        self._rnd15 = numpy.random.random()
        self._rnd16 = numpy.random.random()
        self._rnd17 = numpy.random.random()
        self._rnd18 = numpy.random.random()
        self._rnd19 = numpy.random.random()
        self._rnd20 = numpy.random.random()
        self._rnd100 = numpy.zeros(100)
        foo = 0
        while foo < 0.75:
            foo = numpy.random.random()
        self._rnd100[76] = foo
        self._rnd1000 = numpy.zeros((1000, 1000))
        foo = 0
        while foo < 0.75:
            foo = numpy.random.random()
        self._rnd1000[76][76] = foo

        self.set_state(DevState.ON)
        # PROTECTED REGION END #    //  RandomData.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(RandomData.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  RandomData.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(RandomData.delete_device) ENABLED START #
        self.set_state(DevState.OFF)
        # PROTECTED REGION END #    //  RandomData.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_rnd1(self):
        # PROTECTED REGION ID(RandomData.rnd1_read) ENABLED START #
        """Return the rnd1 attribute."""
        self._rnd1 = numpy.random.random()
        return self._rnd1
        # PROTECTED REGION END #    //  RandomData.rnd1_read

    def read_rnd2(self):
        # PROTECTED REGION ID(RandomData.rnd2_read) ENABLED START #
        """Return the rnd2 attribute."""
        self._rnd2 = numpy.random.random()
        return self._rnd2
        # PROTECTED REGION END #    //  RandomData.rnd2_read

    def read_rnd3(self):
        # PROTECTED REGION ID(RandomData.rnd3_read) ENABLED START #
        """Return the rnd3 attribute."""
        self._rnd3 = numpy.random.random()
        return self._rnd3
        # PROTECTED REGION END #    //  RandomData.rnd3_read

    def read_rnd4(self):
        # PROTECTED REGION ID(RandomData.rnd4_read) ENABLED START #
        """Return the rnd4 attribute."""
        self._rnd4 = numpy.random.random()
        return self._rnd4
        # PROTECTED REGION END #    //  RandomData.rnd4_read

    def read_rnd5(self):
        # PROTECTED REGION ID(RandomData.rnd5_read) ENABLED START #
        """Return the rnd5 attribute."""
        self._rnd5 = numpy.random.random()
        return self._rnd5
        # PROTECTED REGION END #    //  RandomData.rnd5_read

    def read_rnd6(self):
        # PROTECTED REGION ID(RandomData.rnd6_read) ENABLED START #
        """Return the rnd6 attribute."""
        self._rnd6 = numpy.random.random()
        return self._rnd6
        # PROTECTED REGION END #    //  RandomData.rnd6_read

    def read_rnd7(self):
        # PROTECTED REGION ID(RandomData.rnd7_read) ENABLED START #
        """Return the rnd7 attribute."""
        self._rnd7 = numpy.random.random()
        return self._rnd7
        # PROTECTED REGION END #    //  RandomData.rnd7_read

    def read_rnd8(self):
        # PROTECTED REGION ID(RandomData.rnd8_read) ENABLED START #
        """Return the rnd8 attribute."""
        self._rnd8 = numpy.random.random()
        return self._rnd8
        # PROTECTED REGION END #    //  RandomData.rnd8_read

    def read_rnd9(self):
        # PROTECTED REGION ID(RandomData.rnd9_read) ENABLED START #
        """Return the rnd9 attribute."""
        self._rnd9 = numpy.random.random()
        return self._rnd9
        # PROTECTED REGION END #    //  RandomData.rnd9_read

    def read_rnd10(self):
        # PROTECTED REGION ID(RandomData.rnd10_read) ENABLED START #
        """Return the rnd10 attribute."""
        self._rnd10 = numpy.random.random()
        return self._rnd10
        # PROTECTED REGION END #    //  RandomData.rnd10_read

    def read_rnd11(self):
        # PROTECTED REGION ID(RandomData.rnd11_read) ENABLED START #
        """Return the rnd11 attribute."""
        self._rnd11 = numpy.random.random()
        return self._rnd11
        # PROTECTED REGION END #    //  RandomData.rnd11_read

    def read_rnd12(self):
        # PROTECTED REGION ID(RandomData.rnd12_read) ENABLED START #
        """Return the rnd12 attribute."""
        self._rnd12 = numpy.random.random()
        return self._rnd12
        # PROTECTED REGION END #    //  RandomData.rnd12_read

    def read_rnd13(self):
        # PROTECTED REGION ID(RandomData.rnd13_read) ENABLED START #
        """Return the rnd13 attribute."""
        self._rnd13 = numpy.random.random()
        return self._rnd13
        # PROTECTED REGION END #    //  RandomData.rnd13_read

    def read_rnd14(self):
        # PROTECTED REGION ID(RandomData.rnd14_read) ENABLED START #
        """Return the rnd14 attribute."""
        self._rnd14 = numpy.random.random()
        return self._rnd14
        # PROTECTED REGION END #    //  RandomData.rnd14_read

    def read_rnd15(self):
        # PROTECTED REGION ID(RandomData.rnd15_read) ENABLED START #
        """Return the rnd15 attribute."""
        self._rnd15 = numpy.random.random()
        return self._rnd15
        # PROTECTED REGION END #    //  RandomData.rnd15_read

    def read_rnd16(self):
        # PROTECTED REGION ID(RandomData.rnd16_read) ENABLED START #
        """Return the rnd16 attribute."""
        self._rnd16 = numpy.random.random()
        return self._rnd16
        # PROTECTED REGION END #    //  RandomData.rnd16_read

    def read_rnd17(self):
        # PROTECTED REGION ID(RandomData.rnd17_read) ENABLED START #
        """Return the rnd17 attribute."""
        self._rnd17 = numpy.random.random()
        return self._rnd17
        # PROTECTED REGION END #    //  RandomData.rnd17_read

    def read_rnd18(self):
        # PROTECTED REGION ID(RandomData.rnd18_read) ENABLED START #
        """Return the rnd18 attribute."""
        self._rnd18 = numpy.random.random()
        return self._rnd18
        # PROTECTED REGION END #    //  RandomData.rnd18_read

    def read_rnd19(self):
        # PROTECTED REGION ID(RandomData.rnd19_read) ENABLED START #
        """Return the rnd19 attribute."""
        self._rnd19 = numpy.random.random()
        return self._rnd19
        # PROTECTED REGION END #    //  RandomData.rnd19_read

    def read_rnd20(self):
        # PROTECTED REGION ID(RandomData.rnd20_read) ENABLED START #
        """Return the rnd20 attribute."""
        self._rnd20 = numpy.random.random()
        return self._rnd20
        # PROTECTED REGION END #    //  RandomData.rnd20_read

    def read_rnd100(self):
        # PROTECTED REGION ID(RandomData.rnd100_read) ENABLED START #
        """Return the rnd100 attribute."""
        self._rnd100 = numpy.zeros(100)
        foo = 0
        while foo < 0.75:
            foo = numpy.random.random()
        self._rnd100[76] = foo
        return self._rnd100
        # PROTECTED REGION END #    //  RandomData.rnd100_read

    def read_rnd1000(self):
        # PROTECTED REGION ID(RandomData.rnd1000_read) ENABLED START #
        """Return the rnd1000 attribute."""
        self._rnd1000 = numpy.zeros((1000, 1000))
        foo = 0
        while foo < 0.75:
            foo = numpy.random.random()
        self._rnd1000[76][76] = foo
        return self._rnd1000
        # PROTECTED REGION END #    //  RandomData.rnd1000_read

    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the RandomData module."""
    # PROTECTED REGION ID(RandomData.main) ENABLED START #
    return run((RandomData,), args=args, **kwargs)
    # PROTECTED REGION END #    //  RandomData.main


if __name__ == '__main__':
    main()
