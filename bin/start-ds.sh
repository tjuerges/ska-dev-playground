#! /usr/bin/env bash

function help()
{
    why="${1}"
    echo -e "*** Cannot start the Python device server.\n${why}\n\n* The Python file for the device server must be the 1st parameter that is provided.\n* The instance of this device server must be the 2nd parameter that is provided."
    exit -1
}

# Check if the mandatory parameters are present:
# ${1}:  device server's Python file
# ${2}:  instance of the device server's executable in the configDB
case ${#} in
    0)
        help "The device server's Python file and the instance are missing."
        ;;
    1)
        help "The device server's instance is missing."
        ;;
    *)
        deviceServer="${1}"
        shift
        instance="${1}"
        shift
        ;;
esac

# Find the path to the device server's Python file that is
# relative to the /hosthome directory (in Podman the user's
# mounted ${HOME}).
# ATTENTION
# This is assuming that the device server's Python file exists
# on the Podman's host in the user's ${HOME} directory.
runThis=$(basename ${deviceServer})
runThis=${runThis//.sh/.py}
if [ -f ${runThis} ]; then
    myDir=${PWD}
else
    myDir=${PWD}/$(dirname ${deviceServer})
fi
deviceServerPath=${myDir/${HOME}/\/hosthome}

# Tango log lines start with a UNIX timestamp. Replace them with the UTC time.
${CONTAINER_ENGINE} exec -it itango python3 ${deviceServerPath}/${runThis} ${instance} -v ${@} | perl -ne 'use Time::Piece; s/^([0-9]+)/gmtime($1)->strftime("%F %T")/e; print;'
