import tango
from datetime import datetime

# Subscribe to events
def subscribe(publisher: tango.DeviceProxy, attr = None):
    def callback(event: tango.EventData):
        now = datetime.utcnow().strftime('%FT%T.%f')[:-3]
        if event.err is False:
            value_or_error = f'value = {event.attr_value.value}'
        else:
            value_or_error = f'error = ({event.errors})'
        print(f'{now} device = {event.device}, attribute = {event.attr_name}, event = {event.event}, {value_or_error}')

    event_types = [tango.EventType.PERIODIC_EVENT, tango.EventType.CHANGE_EVENT, tango.EventType.ARCHIVE_EVENT]
    event_ids = []
    for event_type in event_types:
        if attr is None:
            try:
                event_ids.append([publisher.subscribe_event(attribute, event_type, callback) for attribute in publisher.get_attribute_list()])
            except Exception as e:
                print(f'The {event_type} subscription for attribute {attribute} went somehow wrong. Continuing.')
        elif isinstance(attr, str):
            try:
                event_ids.append([publisher.subscribe_event(attr, event_type, callback)])
            except Exception as e:
                print(f'The {event_type} subscription for attribute {attr} went somehow wrong. Continuing.')
        else:
            try:
                event_ids.append([publisher.subscribe_event(attribute, event_type, callback) for attribute in attr])
            except Exception as e:
                print(f'The {event_type} subscription for attribute {attr} went somehow wrong. Continuing.')
    return event_ids

# Unsubscribe from events
def unsubscribe(publisher: tango.DeviceProxy, event_ids = None):
    if event_ids is not None:
        for event_type in event_ids:
            [publisher.unsubscribe_event(e) for e in event_type]
        return
    print("You need to pass at least one event ID tha you want to unsubscribe from!")
