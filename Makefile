#! /usr/bin/env make

# Before doing anything, make 100% certain that all of the necessary
# environment variables are set.
ifndef PLAYGROUND_DIR
    # Generate the full path to the playgroundrc.sh file
    # for this exact repository.
    PLAYGROUNDRC = $(subst bin,bootstrap/etc/playgroundrc.sh,$(abspath ./bin))
    $(error There are essential Tango playground environment variables missing. You must source the playgroundrc.sh file of this repo first. You can do it like this (paste without quotes): ". $(PLAYGROUNDRC)")
endif

# Store the directory of this Makefile in a variable.
# This will allow us to later know where its location is.
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(notdir $(patsubst %/,%,$(dir $(MAKEPATH))))

# This is where the Dockerfile and compose files are kept.
CONTAINER_DIR := containers
CONTANER_FILES_PATH := $(abspath $(CONTAINER_DIR))
CONTAINER_COMPOSE_ENV_FILE := $(abspath $(CONTANER_FILES_PATH)/.env)
COMPOSE_FILES := $(wildcard $(CONTANER_FILES_PATH)/*.yml)
COMPOSE_FILE_ARGS := --env-file $(CONTAINER_COMPOSE_ENV_FILE) $(foreach yml,$(COMPOSE_FILES),-f $(yml))

ATTACH_COMPOSE_FILE_ARGS := $(foreach yml,$(filter-out $(CONTAINER_DIR)/tango.yml,$(COMPOSE_FILES)),-f $(yml))

# If the container engine is docker, use its built in docker compose.
ifeq ($(CONTAINER_ENGINE),docker)
    COMPOSE_ENGINE := docker compose
else
    COMPOSE_ENGINE := docker-compose
endif

# How many parallel container build jobs?
# The default is set in playgroundrc.sh.
COMPOSE_BUILD_JOBS ?= $(shell nproc)

# The tango playground's VM name
PLAYGROUND_VM_NAME ?= tango-playground

# The default network mode is tangonet.
#
# Note: This is automatically set if you source bootstrap/etc/playgroundrc.sh.
#
# The "host" network mode will make the tangodb and archiverdb ports
# clash because they both have mariaDB running on port 3306.
# But we allow to overwrite it.
NETWORK_MODE ?= tangonet

# Host name through which others can reach our control interfaces.
# Needs to be resolvable from the containers and clients.
ifneq (,$(wildcard /run/WSL))
    # Microsoft Windows Subsystem for Linux
ifeq ($(CONTAINER_ENGINE),podman)
    HOSTNAME ?= host.podman.internal
else

    HOSTNAME ?= host.docker.internal
endif
else
    HOSTNAME ?= $(shell hostname -f)
endif

# Host name to which to send our container logs. Needs to be resolvable from
# the host.
#
# Note: This is automatically set if you source bootstrap/etc/playgroundrc.sh.
#
LOG_HOSTNAME ?= localhost

# If the first make argument is "start" or "stop"...
ifeq (start,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
else ifeq (stop,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
else ifeq (restart,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
else ifeq (up,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
else ifeq (build,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
else ifeq (build-nocache,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
else ifeq (attach,$(firstword $(MAKECMDGOALS)))
    SERVICE_TARGET = true
    ifndef NETWORK_MODE
        $(error NETWORK_MODE must specify the network to attach to, e.g., make NETWORK_MODE=tangonet-powersupply ...)
    endif

    ifndef TANGO_HOST
        $(error TANGO_HOST must specify the Tango database device, e.g., make TANGO_HOST=powersupply-databaseds:10000 ...)
    endif
endif

ifdef SERVICE_TARGET
    # .. then use the rest as arguments for the make target
    SERVICE := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
    # ...and turn them into do-nothing targets
    $(eval $(SERVICE):;@:)
endif

#
# Never use the network=host mode when running CI jobs, and add extra
# distinguishing identifiers to the network name and container names to
# prevent collisions with jobs from the same project running at the same
# time.
#
ifneq ($(CI_JOB_ID),)
    NETWORK_MODE := tangonet-$(CI_JOB_ID)
    CONTAINER_NAME_PREFIX := $(CI_JOB_ID)-
else
    CONTAINER_NAME_PREFIX :=
endif

ifneq ($(OS),Windows_NT)
    UNAME_S := $(shell uname -s)

    # On Linux and macOS ${HOME} will be mounted to /hosthome.
    # This is needed for X11 apps.
    HOSTHOME := $(HOME):/hosthome:rw

    CONTAINER_EXECUTION_UID ?= $(shell id -u)

    ifeq ($(UNAME_S),Linux)
        ifeq ($(CONTAINER_ENGINE),podman)
            CONTAINER_EXECUTION_GID ?= $(shell getent group podman | cut -d: -f 3)
        else
            CONTAINER_EXECUTION_GID ?= $(shell getent group docker | cut -d: -f 3)
        endif
        DISPLAY ?= :0.0
        XAUTHORITY_MOUNT := /tmp/.X11-unix:/tmp/.X11-unix
        XAUTHORITY ?= /hosthome/.Xauthority
        # /bin/sh (=dash) does not evaluate 'podman network' conditionals correctly
        SHELL := /bin/bash
    else ifeq ($(UNAME_S),Darwin)
        CONTAINER_EXECUTION_GID ?= $(shell id -g)
        IF_INTERFACE := $(shell scutil --nwi | grep 'Network interfaces:' | cut -d' ' -f3)
        IP_ADDRESS := $(shell scutil --nwi | grep 'address' | cut -d':' -f2 | tr -d ' ' | head -n1)
        DISPLAY := $(IP_ADDRESS):0
        # Make sure that Darwin, especially from macOS Catalina on,
        # allows X access from our containers.
        ADD_TO_XHOST := $(shell xhost +$(IP_ADDRESS))
        XAUTHORITY_MOUNT := $(HOME)/.Xauthority:/hosthome/.Xauthority:ro
        XAUTHORITY := /hosthome/.Xauthority
    endif
else
    $(error Sorry, but native Windows is not supported.)
endif

# iTango, Pogo and Jive like to have their /home/tango directories.
# The default is to mount ${PLAYGROUND_DIR}/container-scratch as
# /home/tango:rw.
# This either uses the value of ${CONTAINER_SCRATCH}, which is set in
# ${PLAYGROUND_DIR}/bootstrap/etc/playgroundrc.sh, or it assigns a
# default from ${CONTAINER_SCRATCH_DIR} which is again set in
# ${PLAYGROUND_DIR}/bootstrap/etc/playgroundrc.sh.
CONTAINER_SCRATCH ?= $(CONTAINER_SCRATCH_DIR):/home/tango:rw

# When running in network=host mode, point devices at a port on the host
# machine rather than at the container.
ifeq ($(NETWORK_MODE),host)
    TANGO_HOST := $(shell hostname):10000
    MYSQL_HOST := $(shell hostname):3306
else
    ifeq ($(TANGO_HOST),)
        TANGO_HOST := $(CONTAINER_NAME_PREFIX)databaseds:10000
    else
        TANGO_HOST := $(TANGO_HOST)
    endif

    ifeq ($(MYSQL_HOST),)
        MYSQL_HOST := $(CONTAINER_NAME_PREFIX)tangodb:3306
    else
        MYSQL_HOST := $(MYSQL_HOST)
    endif
endif

COMPOSE_ENGINE_ARGS := DISPLAY=$(DISPLAY) \
    CONTAINER_SCRATCH=$(CONTAINER_SCRATCH) \
    XAUTHORITY=$(XAUTHORITY) \
    TANGO_HOST=$(TANGO_HOST) \
    MYSQL_HOST=$(MYSQL_HOST) \
    HOSTHOME=$(HOSTHOME) \
    HOSTNAME=$(HOSTNAME) \
    LOG_HOSTNAME=$(LOG_HOSTNAME) \
    NETWORK_MODE=$(NETWORK_MODE) \
    XAUTHORITY_MOUNT=$(XAUTHORITY_MOUNT) \
    CONTAINER_NAME_PREFIX=$(CONTAINER_NAME_PREFIX) \
    COMPOSE_IGNORE_ORPHANS=true \
    CONTAINER_EXECUTION_UID=$(CONTAINER_EXECUTION_UID) \
    CONTAINER_EXECUTION_GID=$(CONTAINER_EXECUTION_GID)

# Add the DOCKER_HOST export if podman is the container engine.
# Otherwise docker-compose does not work nicely with podman.
ifeq ($(CONTAINER_ENGINE),podman)
    COMPOSE_ENGINE_ARGS += \
    DOCKER_HOST=$(DOCKER_HOST)
endif

.PHONY: up down minimal start stop restart build build-nocache status clean pull help
.DEFAULT_GOAL := help

scratch:  ## Create the scratch directory for the settings and configs.
	@([ ! -d ${CONTAINER_SCRATCH_DIR} ]) && mkdir -p ${CONTAINER_SCRATCH_DIR} || true

venv:  ## Create a Python3 venv in ${PLAYGROUND_DIR}/venv
	$(PLAYGROUND_DIR)/bootstrap/bin/createPythonEnv.sh || true

podman_init:  ## initialise the podman VM
ifeq ($(CONTAINER_ENGINE),podman)
	# Only execute this if podman is the selected container engine.
	# After creation, the VM needs to be restarted because sometimes the
	# podman client fails to connect.
	if [ $$(podman machine list --noheading --format "{{.Name}}" | grep -c $(PLAYGROUND_VM_NAME)) -eq 0 ]; then podman machine init --rootful --image-path stable --cpus 4 --disk-size 60 --memory 4096 --volume $(HOME):$(HOME) $(PLAYGROUND_VM_NAME); podman machine stop $(PLAYGROUND_VM_NAME); sleep 10; podman machine start $(PLAYGROUND_VM_NAME); fi
endif

podman_exit:  ## stop the podman VM
ifeq ($(CONTAINER_ENGINE),podman)
	# Only execute this if podman is the selected container engine.
	if [ $$(podman machine list --noheading --format "{{.Name}}" | grep -c $(PLAYGROUND_VM_NAME)) -eq 1 ]; then podman machine stop $(PLAYGROUND_VM_NAME); fi
endif

podman_start: podman_init  ## start the podman VM
ifeq ($(CONTAINER_ENGINE),podman)
	# Only execute this if podman is the selected container engine.
	if [ $$(podman machine list --noheading --format "{{.Name}}:{{.LastUp}}" | grep -c "$(PLAYGROUND_VM_NAME):Currently running") -eq 0 ]; then podman machine start $(PLAYGROUND_VM_NAME); fi
endif

podman_started:  ## Check if the podman VM has been started
ifeq ($(CONTAINER_ENGINE),podman)
	# Only execute this if podman is the selected container engine.
	@[[ $$(podman machine list --noheading --format "{{.Name}}:{{.LastUp}}" | grep -c "$(PLAYGROUND_VM_NAME):Currently running") -eq 1 ]]
endif

container_stop:  ## stop the containers and the network
	@$(MAKE) down || true
	@$(MAKE) container_network_down || true

container_clean: podman_started  ## delete the containers and the VM
	@$(MAKE) container_stop || true
	$(CONTAINER_ENGINE) volume rm $(CONTAINER_DIR)_tangodb ${CONTAINER_DIR}_archive || true
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) rm -f -v -a -s
ifeq ($(CONTAINER_ENGINE),podman)
	# Only execute this if podman is the selected container engine.
	podman machine rm -f $(PLAYGROUND_VM_NAME) || true
endif

container_network_up: podman_started  ## create the container network
ifneq ($(NETWORK_MODE),host)
	$(CONTAINER_ENGINE) network inspect $(NETWORK_MODE) &> /dev/null || ([ $${?} -ne 0 ] && $(CONTAINER_ENGINE) network create $(NETWORK_MODE))
	$(CONTAINER_ENGINE) network inspect 9000-$(NETWORK_MODE) &> /dev/null || ([ $${?} -ne 0 ] && $(CONTAINER_ENGINE) network create 9000-$(NETWORK_MODE) -o mtu=9000)
endif

container_network_down:  ## Remove the container network
ifneq ($(NETWORK_MODE),host)
	$(CONTAINER_ENGINE) network inspect $(NETWORK_MODE) &> /dev/null && ([ $${?} -eq 0 ] && $(CONTAINER_ENGINE) network rm $(NETWORK_MODE)) || true
	$(CONTAINER_ENGINE) network inspect 9000-$(NETWORK_MODE) &> /dev/null && ([ $${?} -eq 0 ] && $(CONTAINER_ENGINE) network rm 9000-$(NETWORK_MODE)) || true
endif

pull: podman_start  ## pull the images from the container hub
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) pull

build: pull  ## rebuild images
	# docker-compose does not support build dependencies, so manage those here.
	#$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) -f some-device-base.yml -f networks.yml build --progress=plain
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) build --parallel $(SERVICE)

build-nocache: pull  ## rebuild images from scratch
	# docker-compose does not support build dependencies, so manage those here.
	#$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) -f some-device-base.yml -f networks.yml build --progress=plain
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) build --parallel --no-cache $(SERVICE)

bootstrap: scratch venv build  ## initialise everything from scratch
	# Boot up containers to load the TangoDB configurations.
	@$(MAKE) start dsconfig || true
	# Wait for dsconfig container to have started.
	@echo "Sleep for 5s to give dsconfig time to connect to the TANGO_HOST..."
	sleep 5
	# Load the Tango playground default configuration. This is the basic
	# configuration that just contains the TangoDB DS and dsconfig.
	updateTangoDB.sh -l -f TangoDB/tango-playground-default.json
	# Now add the archiver configuration. Note the -u parameter!
	updateTangoDB.sh -u -f TangoDB/archiver-devices.json
	# And add the RandomData device. Again, note the -u parameter!
	updateTangoDB.sh -u -f devices/randomData/TangoDB/RandomData.json

clean: down container_clean  ## clear all TANGO database entries, and all containers

up: minimal  ## start the base TANGO system and prepare requested services
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) up --no-start --no-recreate $(SERVICE)

down: stop container_network_down  ## stop all services and tear down the system
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) down

minimal: podman_start container_network_up  ## start the base TANGO system
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) -f $(CONTANER_FILES_PATH)/tango.yml -f $(CONTANER_FILES_PATH)/networks.yml up -d

start: scratch up  ## start a service (usage: make start <servicename>)
	if [ $(UNAME_S) = Linux ]; then touch ~/.Xauthority; chmod a+r ~/.Xauthority; fi
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) start $(SERVICE)

stop:  ## stop a service (usage: make stop <servicename>)
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) stop $(SERVICE)

restart:  ## restart a service (usage: make restart <servicename>)
	# Cannot use dependencies here, as that would allow start and stop to run
	# in parallel.
	@$(MAKE) stop $(SERVICE)
	@$(MAKE) start $(SERVICE)

attach:  ## attach a service to an existing Tango network
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(ATTACH_COMPOSE_FILE_ARGS) up -d $(SERVICE)

status:  ## show the container status
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) ps

logs:  ## show the logs
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) logs -t

images:  ## show the container images
	$(COMPOSE_ENGINE_ARGS) $(COMPOSE_ENGINE) $(COMPOSE_FILE_ARGS) images

volumes:  ## show the container volumes
	$(COMPOSE_ENGINE_ARGS) $(CONTAINER_ENGINE) volume ls

help:  ## show this help.
	@grep -E '^[[:alnum:]_-]+:\s*[[:alnum:]_-]*\s*##\s*[[:alnum:]]+' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sort
	@echo "\nSome settings, in case things go wrong:"
ifeq ($(CONTAINER_ENGINE),podman)
	@echo "* PLAYGROUND_VM_NAME = $(PLAYGROUND_VM_NAME)"
endif
	@echo "* HOSTNAME = $(HOSTNAME)"
	@echo "* HOSTHOME = $(HOSTHOME)"
	@echo "* DISPLAY = $(DISPLAY)"
	@echo "* TANGO_HOST = $(TANGO_HOST)"
	@echo "* MYSQL_HOST = $(MYSQL_HOST)"
	@echo "* CONTAINER_SCRATCH_DIR = $(CONTAINER_SCRATCH_DIR)"
	@echo "* CONTAINER_ENGINE = $(CONTAINER_ENGINE)"
	@echo "* COMPOSE_ENGINE = $(COMPOSE_ENGINE)"
	@echo "* NETWORK_MODE = $(NETWORK_MODE)"
	@echo "* COMPOSE_ENGINE_ARGS = $(COMPOSE_ENGINE_ARGS)"
	@echo "* ATTACH_COMPOSE_FILE_ARGS = $(ATTACH_COMPOSE_FILE_ARGS)"
